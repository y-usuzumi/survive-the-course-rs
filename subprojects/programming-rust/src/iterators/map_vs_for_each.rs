#[cfg(test)]
mod tests {
    #[test]
    fn test_map_vs_for_each() {
        // map converts an iterator into a new iterator. It does not consume the source iterator unless the new iterator is consumed
        let v = vec![1, 2, 3];
        let mut iterator = v.iter();
        let mut result = vec![];
        // You can actually see a warning here:
        //     unused `Map` that must be used
        //     iterators are lazy and do nothing unless consumed
        // Also, we call `by_ref` to borrow the iterator so we can
        // reuse it below.
        iterator.by_ref().map(|elt| {
            result.push(*elt);
            *elt + 100
        });
        assert!(result.is_empty());

        // Since at L14 we did not consume the borrowed iterator,
        // here it will start consuming from position 0.
        iterator.for_each(|elt| {
            result.push(*elt + 200);
        });
        assert_eq!(result, vec![201, 202, 203]);
    }
}
