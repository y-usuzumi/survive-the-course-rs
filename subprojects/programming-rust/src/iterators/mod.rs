mod map_vs_for_each;
mod tree_iter;
mod tree_iter_practice;

#[cfg(test)]
mod tests {
    #[test]
    fn test_drain() {
        let mut v = ["a", "b", "c"].map(str::to_string).to_vec();
        v.drain(..);
        // Drain removes the range from the collection immediately
        assert_ne!(v, ["a", "b", "c"].map(str::to_string).to_vec());
    }

    #[test]
    fn test_filter() {
        let s = "  hello  \n  world  \n\n  ";
        let lines: Vec<_> = s.lines().collect();
        assert_eq!(lines.len(), 4);
        assert_eq!("  hello  ", lines[0]);
        assert_eq!("  world  ", lines[1]);
        assert!(lines[2].is_empty());
        assert_eq!("  ", lines[3]);
    }
}
