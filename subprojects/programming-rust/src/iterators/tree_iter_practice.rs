use self::Tree::*;

enum Tree<T> {
    Empty,
    NonEmpty(Box<TreeNode<T>>),
}

struct TreeNode<T> {
    element: T,
    left: Tree<T>,
    right: Tree<T>,
}

impl<'a, T> IntoIterator for &'a Tree<T> {
    type IntoIter = TreeIter<'a, T>;
    type Item = &'a T;
    fn into_iter(self) -> Self::IntoIter {
        let mut iter = TreeIter { stack: Vec::new() };
        iter.push_left_node(self);
        return iter;
    }
}

struct TreeIter<'a, T> {
    stack: Vec<&'a TreeNode<T>>,
}

impl<'a, T> TreeIter<'a, T> {
    fn push_left_node(&mut self, mut elt: &'a Tree<T>) {
        while let NonEmpty(node) = elt {
            self.stack.push(node);
            elt = &node.left;
        }
    }
}

impl<'a, T> Iterator for TreeIter<'a, T> {
    type Item = &'a T;

    fn next(&mut self) -> Option<Self::Item> {
        if let Some(node) = self.stack.pop() {
            self.push_left_node(&node.right);
            return Some(&node.element);
        }
        return None;
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    impl<T: Ord> Tree<T> {
        fn add(&mut self, value: T) {
            match self {
                Empty => {
                    *self = Tree::NonEmpty(Box::new(TreeNode {
                        element: value,
                        left: Tree::Empty,
                        right: Tree::Empty,
                    }))
                }
                NonEmpty(node) => {
                    if value <= node.element {
                        node.left.add(value);
                    } else {
                        node.right.add(value);
                    }
                }
            }
        }
    }

    #[test]
    fn test_iterator() {
        let mut tree = Tree::Empty;
        tree.add("jaeger");
        tree.add("robot");
        tree.add("droid");
        tree.add("mecha");

        let mut v = Vec::new();
        for kind in &tree {
            v.push(*kind);
        }
        assert_eq!(v, vec!["droid", "jaeger", "mecha", "robot"]);
    }
}
