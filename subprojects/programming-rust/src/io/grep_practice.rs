use std::{
    error::Error,
    fs::File,
    io::{self, BufRead, BufReader},
    path::PathBuf,
};

fn grep<R: BufRead>(target: &str, reader: R) -> io::Result<()> {
    for line_result in reader.lines() {
        let line = line_result?;
        if line.contains(target) {
            println!("{}", line);
        }
    }

    Ok(())
}

fn grep_main() -> Result<(), Box<dyn Error>> {
    // Skip the command name itself
    let mut args = std::env::args().skip(1);
    let target = match args.next() {
        Some(t) => t,
        _ => Err("usage: grep PATTERN FILE...")?,
    };
    let filenames: Vec<PathBuf> = args.map(PathBuf::from).collect();
    if filenames.is_empty() {
        grep(&target, io::stdin().lock())?;
    } else {
        for filename in filenames {
            let f = File::open(filename)?;
            grep(&target, BufReader::new(f))?;
        }
    }

    Ok(())
}
