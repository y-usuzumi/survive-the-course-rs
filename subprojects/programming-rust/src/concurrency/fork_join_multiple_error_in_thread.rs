#[cfg(test)]
mod tests {
    use std::{thread, time::Duration};

    #[test]
    #[ignore = "This test fails purposefully"]
    fn test_fork_join_multiple_error_in_thread() {
        let v = String::from("OK");
        // Note: I thought each closure has its own type, so
        // why does it allow creating a Vec of closures of "different" types?
        let thread_closures = vec![
            move || {
                println!("Thread 1 running");
                thread::sleep(Duration::from_secs(1));
                println!("Thread 1 complete");
                return 1;
            },
            move || {
                println!("Thread 2 running");
                thread::sleep(Duration::from_secs(2));
                panic!("Thread 2 panics");
            },
            move || {
                println!("Thread 3 running");
                thread::sleep(Duration::from_secs(3));
                println!("Thread 3 complete");
                return 3;
            },
        ];
        let handles: Vec<_> = thread_closures
            .into_iter()
            .map(|clj| thread::spawn(clj))
            .collect();
        for (idx, handle) in handles.into_iter().enumerate() {
            let result = handle.join().unwrap();
            println!("Thread {} returns {}", idx + 1, result);
        }
    }
}
