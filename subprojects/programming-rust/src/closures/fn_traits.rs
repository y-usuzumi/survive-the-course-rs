#[cfg(test)]
mod tests {
    #[test]
    fn test_Fn() {
        let s = String::from("Hello");
        let f = || println!("{}", s);
        // Closures that implement `Fn` can be called as many times as you want
        f();
        f();
        // Closures that implement `Fn` also implement `Copy` and `Clone`
        f.clone()();
    }

    #[test]
    fn test_FnMut_no_move() {
        fn consume_s(s: String) {}
        let mut s = String::from("Hello");
        // If a closure holds a
        let mut f = |a| {
            // If a closure holds a mutable reference to a captured variable,
            // it is an FnMut
            s.push_str(a);
            println!("{}", s);
        };
        // Closures that implement `FnMut` also can be called as many times as you want
        f(", world");
        f(", world");
        // This specific closure not implement `Copy` or `Clone` because
        // mutable references do not implement `Copy` or `Clone`
        // f.clone()(", world");
    }

    #[test]
    fn test_FnMut_move() {
        let mut s = String::from("Hello");
        let mut f = move |a| {
            s.push_str(a);
            println!("{}", s);
        };
        f(", world");
        f(", world");
        // f implements `Clone` because:
        // 1. The captured variable s is String, which implements `Clone`
        // 2. s is moved and owned by the closure
        f.clone()(", world");
        f.clone()(", world");
        let g = f;
        // However, f does not implement Copy because String does not
        // implement Copy
        // drop(f);
    }

    #[test]
    fn test_FnOnce() {
        let s = String::from("Hello");
        let f = || s.into_boxed_str();
        // f implements Clone because String implements Clone
        f.clone()();
        f();
        // However, f can only be cloned before being called. This is because
        // s will be moved and cannot be cloned after that
        // f.clone()(); // Err: borrow of moved value
        //
        // It also cannot be called again
        // f(); // Err: use of moved value
    }
}
