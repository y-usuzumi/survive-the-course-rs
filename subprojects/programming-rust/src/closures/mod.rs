mod fn_traits;

use std::{
    collections::HashMap,
    thread::{self, JoinHandle},
};

#[derive(Debug, Clone, Copy)]
enum Statistic {
    Population,
}

struct City {
    name: String,
    population: i64,
    country: String,
}

impl City {
    fn new(name: &str, population: i64, country: &str) -> Self {
        Self {
            name: name.to_string(),
            population,
            country: country.to_string(),
        }
    }
    fn get_statistic(&self, stat: Statistic) -> i64 {
        match stat {
            Statistic::Population => self.population,
        }
    }
}

fn sort_cities(cities: &mut Vec<City>) {
    cities.sort_by_key(|city| -city.population);
}

// Capturing variables

// Closures that borrow
fn sort_by_statistic(cities: &mut Vec<City>, stat: Statistic) {
    cities.sort_by_key(|city| -city.get_statistic(stat));
}

// Closures that steal
fn start_sorting_thread(mut cities: Vec<City>, stat: Statistic) -> JoinHandle<Vec<City>> {
    // Statistic needs to derive Copy because the `sort_by_key` method requires an FnMut closure.
    // Without copy, `sort_key` is an FnOnce because stat can only be stolen once.
    let sort_key = move |city: &City| -city.get_statistic(stat);
    let jh = thread::spawn(move || {
        cities.sort_by_key(sort_key);
        cities
    });
    println!("{:?}", stat);
    return jh;
}

// Closures that kill
fn closure_with_drop(city: &City) {
    let s = "Hello world".to_string();
    let f = || drop(&s);
    f();
    f();
}

fn closure_implicit_reference() {
    let dct = HashMap::from([("a", 1), ("b", 2), ("c", 3)]);
    let f = || {
        // Move can happen (here `dct`) even without using the `move` keyword.
        // Rust will decide when it can go without moving. If it's not possible,
        // it will move anyway.
        for (k, v) in dct {
            println!("{} => {}", k, v);
        }
    };
    f();
    // f(); // Note: f is FnOnce due to the move of `dct`
}

#[cfg(test)]
mod tests {
    use super::{closure_with_drop, City};

    #[test]
    fn test_drop() {
        let city = City::new("Mcti", 12000, "Eunova");
        closure_with_drop(&city);
    }
}
