use std::collections::HashMap;

///! This problem comes from a post in REDnote
/// Given an array arr. Find the number of pairs of elements (arr[i], arr[j]),
/// where arr[i] - arr[j] = i - j

struct Solution;

impl Solution {
    fn find_pairs(arr: &[i32]) -> usize {
        let offset_arr: Vec<i32> = arr
            .iter()
            .enumerate()
            .map(|(idx, elem)| *elem - idx as i32)
            .collect();

        let mut counter: HashMap<i32, usize> = HashMap::new();
        for elem in offset_arr {
            *counter.entry(elem).or_default() += 1;
        }

        let mut result = 0;
        for count in counter.values() {
            // nCr(count, 2)
            result += (count * count - count) / 2;
        }
        return result;
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_1() {
        let arr = vec![1, 2, 3, 4, 5];
        assert_eq!(Solution::find_pairs(&arr), 10);
    }

    #[test]
    fn test_2() {
        let arr = vec![1, 2, 3, 4, 5, 7, 8, 9, 10];
        assert_eq!(Solution::find_pairs(&arr), 16);
    }
}
