// https://leetcode.com/problems/lru-cache/

use std::{
    cell::RefCell,
    collections::HashMap,
    rc::{Rc, Weak},
};

struct Node {
    val: i32,
    prev: Option<Rc<RefCell<Node>>>,
    next: Option<Rc<RefCell<Node>>>,
}

struct LRUCache {
    head: Option<Rc<RefCell<Node>>>,
    last: Option<Rc<RefCell<Node>>>,
    map: HashMap<i32, Weak<RefCell<Node>>>,
    capacity: usize,
}

/**
 * `&self` means the method takes an immutable reference.
 * If you need a mutable reference, change it to `&mut self` instead.
 */
impl LRUCache {
    fn new(capacity: i32) -> Self {
        Self {
            head: None,
            last: None,
            map: HashMap::new(),
            capacity: capacity as usize,
        }
    }

    fn remove(cell: RefCell<Node>) {}

    fn get(&mut self, key: i32) -> i32 {
        if let Some(node) = self.map.get(&key) {
            if let Some(node_rc) = node.upgrade() {
                let mut node_ref = node_rc.borrow_mut();
                let mut prev = node_ref.prev.take();
                let mut next = node_ref.next.take();
                if let Some(prev) = prev {
                    prev.borrow_mut().next = next;
                }
            }
        }

        return 0;
    }

    fn put(&mut self, key: i32, value: i32) {}
}

/**
 * Your LRUCache object will be instantiated and called as such:
 * let obj = LRUCache::new(capacity);
 * let ret_1: i32 = obj.get(key);
 * obj.put(key, value);
 */

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_1() {
        todo!();
    }
}
