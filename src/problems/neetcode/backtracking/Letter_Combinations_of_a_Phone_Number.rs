// https://leetcode.com/problems/letter-combinations-of-a-phone-number/

pub struct Solution;

impl Solution {
    pub fn letter_combinations(digits: String) -> Vec<String> {
        if digits.len() == 0 {
            return vec![];
        }
        let letter_list: Vec<Vec<char>> = vec![
            vec![],
            vec![],
            vec!['a', 'b', 'c'],
            vec!['d', 'e', 'f'],
            vec!['g', 'h', 'i'],
            vec!['j', 'k', 'l'],
            vec!['m', 'n', 'o'],
            vec!['p', 'q', 'r', 's'],
            vec!['t', 'u', 'v'],
            vec!['w', 'x', 'y', 'z'],
        ];

        let digits: Vec<usize> = digits
            .chars()
            .map(|ch| (ch as u8 - '0' as u8) as usize)
            .collect();

        let mut results = vec![vec![]];
        for digit in digits {
            let mut new_results = vec![];
            for result in &results {
                for letter in &letter_list[digit] {
                    let mut new_result = result.clone();
                    new_result.push(*letter);
                    new_results.push(new_result);
                }
            }
            results = new_results;
        }

        return results.iter().map(|chs| chs.iter().collect()).collect();
    }
}

#[cfg(test)]
mod tests {
    use super::*;
}
