// https://leetcode.com/problems/palindrome-partitioning/

pub struct Solution;

impl Solution {
    pub fn partition(s: String) -> Vec<Vec<String>> {
        fn is_palindrome(s: &str, dp: &mut Vec<Vec<i32>>, start: usize, end: usize) -> bool {
            if dp[start][end] > -1 {
                return dp[start][end] == 1;
            }
            let result;
            if start >= end {
                result = true;
            } else {
                result = s.chars().nth(start) == s.chars().nth(end)
                    && is_palindrome(s, dp, start + 1, end - 1);
            }
            dp[start][end] = if result { 1 } else { 0 };
            return result;
        }

        fn backtrack(
            result: &mut Vec<Vec<String>>,
            dp: &mut Vec<Vec<i32>>,
            stack: &mut Vec<String>,
            s: &str,
            start: usize,
        ) {
            if start == s.len() {
                result.push(stack.clone());
                return;
            }

            for end in start..s.len() {
                if is_palindrome(s, dp, start, end) {
                    stack.push(s[start..=end].to_string());
                    backtrack(result, dp, stack, s, end + 1);
                    stack.pop();
                }
            }
        }

        let mut dp = vec![vec![-1; s.len()]; s.len()];
        let mut result = vec![];

        backtrack(&mut result, &mut dp, &mut vec![], &s, 0);

        return result;
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_1() {
        let result = Solution::partition("aab".to_string());
        test_util::assert_eq_ignore_order(
            result,
            vec![
                vec!["a".to_string(), "a".to_string(), "b".to_string()],
                vec!["aa".to_string(), "b".to_string()],
            ],
        );
    }
}
