// https://leetcode.com/problems/n-queens/

pub struct Solution;

impl Solution {
    pub fn solve_n_queens(n: i32) -> Vec<Vec<String>> {
        fn backtrack(
            results: &mut Vec<Vec<usize>>,
            occupied_cols: &mut Vec<bool>,
            occupied_diagps: &mut Vec<bool>,
            occupied_diagns: &mut Vec<bool>,
            curr_row_idx: usize,
            stack: &mut Vec<usize>,
            n: usize,
        ) {
            if curr_row_idx == n {
                results.push(stack.clone());
            }

            for col_idx in 0..n {
                if occupied_cols[col_idx]
                    || occupied_diagps[curr_row_idx + col_idx]
                    || occupied_diagns[col_idx + n - curr_row_idx - 1]
                {
                    continue;
                }

                occupied_cols[col_idx] = true;
                occupied_diagps[curr_row_idx + col_idx] = true;
                occupied_diagns[col_idx + n - curr_row_idx - 1] = true;
                stack.push(col_idx);
                backtrack(
                    results,
                    occupied_cols,
                    occupied_diagps,
                    occupied_diagns,
                    curr_row_idx + 1,
                    stack,
                    n,
                );
                occupied_cols[col_idx] = false;
                occupied_diagps[curr_row_idx + col_idx] = false;
                occupied_diagns[col_idx + n - curr_row_idx - 1] = false;
                stack.pop();
            }
        }

        fn display(result: &Vec<usize>) -> Vec<String> {
            let n = result.len();
            let mut display_result = vec![];
            for row in result {
                let mut display_row = String::new();
                for idx in 0..n {
                    if idx == *row {
                        display_row.push('Q');
                    } else {
                        display_row.push('.');
                    }
                }
                display_result.push(display_row);
            }

            return display_result;
        }

        let n = n as usize;
        let mut results: Vec<Vec<usize>> = vec![];
        let mut occupied_cols = vec![false; n];
        let mut occupied_diagps = vec![false; 2 * n - 1];
        let mut occupied_diagns = vec![false; 2 * n - 1];

        backtrack(
            &mut results,
            &mut occupied_cols,
            &mut occupied_diagps,
            &mut occupied_diagns,
            0,
            &mut vec![],
            n,
        );

        return results.into_iter().map(|result| display(&result)).collect();
    }
}

#[cfg(test)]
mod tests {
    use super::*;
}
