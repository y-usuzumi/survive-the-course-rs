// https://leetcode.cn/problems/subsets-ii/

pub struct Solution;

impl Solution {
    pub fn subsets_with_dup(mut nums: Vec<i32>) -> Vec<Vec<i32>> {
        nums.sort();

        fn backtrack(
            result: &mut Vec<Vec<i32>>,
            curr_nums: &mut Vec<i32>,
            nums: &Vec<i32>,
            idx: usize,
        ) {
            if idx >= nums.len() {
                result.push(curr_nums.clone());
                return;
            }
            curr_nums.push(nums[idx]);
            backtrack(result, curr_nums, nums, idx + 1);
            curr_nums.pop();
            let mut next_idx = idx + 1;
            while next_idx < nums.len() && nums[next_idx] == nums[idx] {
                next_idx += 1;
            }
            backtrack(result, curr_nums, nums, next_idx);
        }

        let mut result = Vec::new();

        backtrack(&mut result, &mut Vec::new(), &mut nums, 0);

        return result;
    }
}

#[cfg(test)]
mod tests {
    use super::*;
}
