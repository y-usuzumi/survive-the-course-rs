// https://leetcode.com/problems/word-search/

pub struct Solution;

impl Solution {
    pub fn exist(board: Vec<Vec<char>>, word: String) -> bool {
        fn backtrack(
            board: &Vec<Vec<char>>,
            visited: &mut Vec<Vec<bool>>,
            word: &Vec<char>,
            idx: usize,
            row: i32,
            col: i32,
        ) -> bool {
            if idx == word.len() {
                return true;
            }

            if row < 0 || row >= board.len() as i32 {
                return false;
            }
            if col < 0 || col >= board[0].len() as i32 {
                return false;
            }

            if visited[row as usize][col as usize] {
                return false;
            }

            let ch = board[row as usize][col as usize];
            if ch != word[idx] {
                return false;
            }

            visited[row as usize][col as usize] = true;
            if backtrack(board, visited, word, idx + 1, row - 1, col)
                || backtrack(board, visited, word, idx + 1, row, col + 1)
                || backtrack(board, visited, word, idx + 1, row + 1, col)
                || backtrack(board, visited, word, idx + 1, row, col - 1)
            {
                return true;
            }
            visited[row as usize][col as usize] = false;
            return false;
        }
        let mut visited = vec![vec![false; board[0].len()]; board.len()];
        for row in 0..board.len() {
            for col in 0..board[0].len() {
                if backtrack(
                    &board,
                    &mut visited,
                    &word.chars().collect(),
                    0,
                    row as i32,
                    col as i32,
                ) {
                    return true;
                }
            }
        }
        return false;
    }
}

#[cfg(test)]
mod tests {
    use super::*;
}
