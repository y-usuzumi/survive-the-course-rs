// https://leetcode.com/problems/combination-sum-ii/

pub struct Solution;

impl Solution {
    pub fn combination_sum2(mut candidates: Vec<i32>, target: i32) -> Vec<Vec<i32>> {
        candidates.sort();
        let mut result = vec![];
        // TODO: No need for both target and curr_sum. We can subtract the current
        // number from target as we call `backtrack`.
        fn backtrack(
            result: &mut Vec<Vec<i32>>,
            target: i32,
            stack: &mut Vec<i32>,
            curr_sum: i32,
            remaining: &[i32],
        ) {
            if curr_sum == target {
                result.push(stack.clone());
                return;
            }
            if curr_sum > target {
                return;
            }

            for idx in 0..remaining.len() {
                let curr = remaining[idx];
                if idx > 0 && curr == remaining[idx - 1] {
                    continue;
                }
                stack.push(curr);
                backtrack(
                    result,
                    target,
                    stack,
                    curr_sum + curr,
                    &remaining[idx + 1..],
                );
                stack.pop();
            }
        }
        backtrack(&mut result, target, &mut vec![], 0, &candidates);
        return result;
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_1() {
        let cands = vec![10, 1, 2, 7, 6, 1, 5];
        let expected = vec![vec![1, 1, 6], vec![1, 2, 5], vec![1, 7], vec![2, 6]];
        test_util::assert_eq_ignore_order(Solution::combination_sum2(cands, 8), expected);
    }

    #[test]
    fn test_2() {
        let cands = vec![2, 5, 2, 1, 2];
        let expected = vec![vec![1, 2, 2], vec![5]];
        test_util::assert_eq_ignore_order(Solution::combination_sum2(cands, 5), expected);
    }
}
