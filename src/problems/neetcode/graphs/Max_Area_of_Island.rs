// https://leetcode.com/problems/max-area-of-island/
use std::collections::VecDeque;

pub struct Solution;

impl Solution {
    pub fn max_area_of_island(grid: Vec<Vec<i32>>) -> i32 {
        let rows = grid.len();
        let cols = grid[0].len();
        let mut visited = vec![vec![false; cols]; rows];
        let mut result = 0;
        for row_idx in 0..rows {
            for col_idx in 0..cols {
                if grid[row_idx][col_idx] == 0 || visited[row_idx][col_idx] {
                    continue;
                }
                let mut q = VecDeque::from([(row_idx, col_idx)]);
                let mut curr_result = 0;
                while let Some((row_idx, col_idx)) = q.pop_front() {
                    if grid[row_idx][col_idx] == 0 || visited[row_idx][col_idx] {
                        continue;
                    }
                    visited[row_idx][col_idx] = true;
                    curr_result += 1;
                    if row_idx > 0 {
                        q.push_back((row_idx - 1, col_idx));
                    }
                    if row_idx < rows - 1 {
                        q.push_back((row_idx + 1, col_idx));
                    }
                    if col_idx > 0 {
                        q.push_back((row_idx, col_idx - 1));
                    }
                    if col_idx < cols - 1 {
                        q.push_back((row_idx, col_idx + 1));
                    }
                }
                result = result.max(curr_result);
            }
        }
        return result;
    }
}

#[cfg(test)]
mod tests {
    use super::*;
}
