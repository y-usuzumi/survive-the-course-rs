// https://leetcode.com/problems/pacific-atlantic-water-flow/
use std::{
    cmp::Reverse,
    collections::{BinaryHeap, HashSet},
};

pub struct Solution;

impl Solution {
    pub fn pacific_atlantic(heights: Vec<Vec<i32>>) -> Vec<Vec<i32>> {
        let rows = heights.len();
        let cols = heights[0].len();
        let mut pacific_level = 0;
        let mut pacific_h = BinaryHeap::new();
        let mut pacific_cells = HashSet::new();
        let mut pacific_visited = vec![vec![false; cols]; rows];
        for col in 0..cols {
            pacific_h.push(Reverse((heights[0][col], 0, col)));
        }
        for row in 1..rows {
            pacific_h.push(Reverse((heights[row][0], row, 0)));
        }

        while let Some(Reverse((height, row_idx, col_idx))) = pacific_h.pop() {
            if height < pacific_level || pacific_visited[row_idx][col_idx] {
                continue;
            }
            pacific_visited[row_idx][col_idx] = true;
            pacific_cells.insert((row_idx, col_idx));
            pacific_level = height;
            if row_idx > 0 {
                pacific_h.push(Reverse((
                    heights[row_idx - 1][col_idx],
                    row_idx - 1,
                    col_idx,
                )));
            }
            if row_idx < rows - 1 {
                pacific_h.push(Reverse((
                    heights[row_idx + 1][col_idx],
                    row_idx + 1,
                    col_idx,
                )));
            }
            if col_idx > 0 {
                pacific_h.push(Reverse((
                    heights[row_idx][col_idx - 1],
                    row_idx,
                    col_idx - 1,
                )));
            }
            if col_idx < cols - 1 {
                pacific_h.push(Reverse((
                    heights[row_idx][col_idx + 1],
                    row_idx,
                    col_idx + 1,
                )));
            }
        }

        let mut atlantic_level = 0;
        let mut atlantic_h = BinaryHeap::new();
        let mut atlantic_visited = vec![vec![false; cols]; rows];
        for col in 0..cols {
            atlantic_h.push(Reverse((heights[rows - 1][col], rows - 1, col)));
        }
        for row in 0..rows - 1 {
            atlantic_h.push(Reverse((heights[row][cols - 1], row, cols - 1)));
        }

        let mut results = vec![];
        while let Some(Reverse((height, row_idx, col_idx))) = atlantic_h.pop() {
            if height < atlantic_level || atlantic_visited[row_idx][col_idx] {
                continue;
            }
            atlantic_visited[row_idx][col_idx] = true;
            if pacific_cells.contains(&(row_idx, col_idx)) {
                results.push(vec![row_idx as i32, col_idx as i32]);
            }
            atlantic_level = height;
            if row_idx > 0 {
                atlantic_h.push(Reverse((
                    heights[row_idx - 1][col_idx],
                    row_idx - 1,
                    col_idx,
                )));
            }
            if row_idx < rows - 1 {
                atlantic_h.push(Reverse((
                    heights[row_idx + 1][col_idx],
                    row_idx + 1,
                    col_idx,
                )));
            }
            if col_idx > 0 {
                atlantic_h.push(Reverse((
                    heights[row_idx][col_idx - 1],
                    row_idx,
                    col_idx - 1,
                )));
            }
            if col_idx < cols - 1 {
                atlantic_h.push(Reverse((
                    heights[col_idx][col_idx + 1],
                    row_idx,
                    col_idx + 1,
                )));
            }
        }

        return results;
    }
}

#[cfg(test)]
mod tests {
    use super::*;
}
