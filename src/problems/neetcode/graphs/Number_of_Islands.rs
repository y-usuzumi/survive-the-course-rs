// https://leetcode.com/problems/number-of-islands/
use std::collections::VecDeque;

pub struct Solution;

impl Solution {
    pub fn num_islands(grid: Vec<Vec<char>>) -> i32 {
        let rows = grid.len();
        let cols = grid[0].len();
        let mut q = VecDeque::new();
        let mut visited = vec![vec![false; cols]; rows];
        let mut result = 0;
        for row_idx in 0..rows {
            for col_idx in 0..cols {
                if grid[row_idx][col_idx] == '0' || visited[row_idx][col_idx] {
                    continue;
                }
                result += 1;
                q.push_back((row_idx, col_idx));
                while let Some((row_idx, col_idx)) = q.pop_front() {
                    if grid[row_idx][col_idx] == '0' || visited[row_idx][col_idx] {
                        continue;
                    }
                    visited[row_idx][col_idx] = true;
                    if row_idx > 0 {
                        q.push_back((row_idx - 1, col_idx));
                    }
                    if row_idx < rows - 1 {
                        q.push_back((row_idx + 1, col_idx));
                    }
                    if col_idx > 0 {
                        q.push_back((row_idx, col_idx - 1));
                    }
                    if col_idx < cols - 1 {
                        q.push_back((row_idx, col_idx + 1));
                    }
                }
            }
        }
        return result;
    }
}

#[cfg(test)]
mod tests {
    use super::*;
}
