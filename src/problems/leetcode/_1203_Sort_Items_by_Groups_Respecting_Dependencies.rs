// https://leetcode.com/problems/sort-items-by-groups-respecting-dependencies/

use std::collections::{HashMap, HashSet};

type DepGraph = HashMap<i32, Vec<i32>>;

pub struct Solution;

impl Solution {
    const GID_MIN_BOUND: i32 = 10000;
    #[inline]
    fn get_gid(gid: i32) -> i32 {
        return gid + Self::GID_MIN_BOUND;
    }
    pub fn sort_items(n: i32, m: i32, groups: Vec<i32>, before_items: Vec<Vec<i32>>) -> Vec<i32> {
        let mut outer_dep_graph = DepGraph::new();
        let mut inner_dep_graphs = vec![DepGraph::new(); m as usize];
        for (idx, (&group, before_items)) in groups.iter().zip(&before_items).enumerate() {
            let idx = idx as i32;
            if group == -1 {
                outer_dep_graph.entry(idx).or_default();
                for &before_item in before_items {
                    let item_group = groups[before_item as usize];
                    if item_group == -1 {
                        outer_dep_graph.entry(before_item).or_default().push(idx);
                    } else {
                        outer_dep_graph
                            .entry(Self::get_gid(item_group))
                            .or_default()
                            .push(idx);
                    }
                }
            } else {
                outer_dep_graph.entry(Self::get_gid(group)).or_default();
                inner_dep_graphs[group as usize].entry(idx).or_default();
                for &before_item in before_items {
                    let item_group = groups[before_item as usize];
                    if item_group == -1 {
                        outer_dep_graph
                            .entry(before_item)
                            .or_default()
                            .push(Self::get_gid(group));
                    } else if item_group == group {
                        inner_dep_graphs[group as usize]
                            .entry(before_item)
                            .or_default()
                            .push(idx);
                    } else {
                        outer_dep_graph
                            .entry(Self::get_gid(item_group))
                            .or_default()
                            .push(Self::get_gid(group));
                    }
                }
            }
        }

        fn toposort(dep_graph: &DepGraph) -> Vec<i32> {
            fn dfs(
                stack: &mut Vec<i32>,
                visited: &mut HashSet<i32>,
                dep_graph: &DepGraph,
                curr_node: i32,
            ) {
                if visited.contains(&curr_node) {
                    return;
                }

                visited.insert(curr_node);
                for &child in dep_graph.get(&curr_node).unwrap() {
                    dfs(stack, visited, dep_graph, child);
                }
                stack.push(curr_node);
            }
            let mut stack = vec![];
            let mut visited = HashSet::new();
            for (&node, _) in dep_graph {
                dfs(&mut stack, &mut visited, dep_graph, node);
            }

            return stack.into_iter().rev().collect();
        }

        fn has_cycle(toposort_result: &Vec<i32>, dep_graph: &DepGraph) -> bool {
            let mut indices_map = HashMap::new();
            for (idx, node) in toposort_result.iter().enumerate() {
                indices_map.insert(node, idx);
            }
            for (parent, children) in dep_graph {
                for child in children {
                    if indices_map.get(parent).unwrap() > indices_map.get(child).unwrap() {
                        return true;
                    }
                }
            }
            return false;
        }

        let temp_result = toposort(&outer_dep_graph);
        if has_cycle(&temp_result, &outer_dep_graph) {
            return vec![];
        }
        let mut result = vec![];
        for node in temp_result {
            if node >= Self::GID_MIN_BOUND {
                let inner_dep_graph = inner_dep_graphs
                    .get((node - Self::GID_MIN_BOUND) as usize)
                    .unwrap();
                let group_result = toposort(inner_dep_graph);
                if has_cycle(&group_result, &inner_dep_graph) {
                    return vec![];
                }
                result.extend(group_result);
            } else {
                result.push(node);
            }
        }
        return result;
    }
}

#[cfg(test)]
mod tests {
    use super::*;
}
