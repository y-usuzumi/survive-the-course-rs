// https://leetcode.com/problems/best-time-to-buy-and-sell-stock-iv/

// 2D DP
pub struct Solution1;
pub struct Solution2;

impl Solution1 {
    pub fn max_profit(k: i32, prices: Vec<i32>) -> i32 {
        let k = k as usize;
        let mut dp = vec![vec![-1; k]; prices.len()];
        fn helper(dp: &mut Vec<Vec<i32>>, prices: &Vec<i32>, start: usize, k: usize) -> i32 {
            if prices.is_empty() || k == 0 {
                return 0;
            }

            if dp[start][k - 1] > 0 {
                return dp[start][k - 1];
            }

            let mut result = 0;
            let mut left_max = 0;
            let mut curr_min = i32::MAX;
            for idx in start..prices.len() {
                curr_min = curr_min.min(prices[idx]);
                left_max = left_max.max(prices[idx] - curr_min);
                let right_result = helper(dp, prices, idx, k - 1);
                result = result.max(left_max + right_result);
            }

            dp[start][k - 1] = result;
            return result;
        }
        return helper(&mut dp, &prices, 0, k);
    }
}

impl Solution2 {
    pub fn max_profit(k: i32, prices: Vec<i32>) -> i32 {
        todo!();
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_helloworld() {
        let prices = vec![3, 2, 6, 5, 0, 3];
        println!("{:?}", Solution1::max_profit(2, prices));
    }
}
