// https://leetcode.com/problems/best-time-to-buy-and-sell-stock-iii/

pub struct Solution;

impl Solution {
    pub fn max_profit(prices: Vec<i32>) -> i32 {
        if prices.is_empty() {
            return 0;
        }
        let count = prices.len();
        let mut trans = vec![];
        let mut last_price = prices[0];
        let mut start_idx = 0;
        for idx in 1..count {
            let price = prices[idx];
            if price < last_price {
                if idx - 1 > start_idx {
                    trans.push((prices[start_idx], last_price));
                }
                last_price = price;
                start_idx = idx;
            } else {
                last_price = price;
            }
        }
        if count - 1 > start_idx {
            trans.push((prices[start_idx], prices[count - 1]));
        }

        return 0;
    }
}

#[cfg(test)]
mod tests {
    use super::*;
}
