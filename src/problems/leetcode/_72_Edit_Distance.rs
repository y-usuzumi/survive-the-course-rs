// https://leetcode.com/problems/edit-distance/

pub struct Solution;

impl Solution {
    pub fn min_distance(word1: String, word2: String) -> i32 {
        let mut dp = vec![vec![0; word2.len() + 1]; word1.len() + 1];
        let word1: Vec<char> = word1.chars().collect();
        let word2: Vec<char> = word2.chars().collect();
        for idx in 0..=word2.len() {
            dp[0][idx] = idx;
        }
        for idx in 0..=word1.len() {
            dp[idx][0] = idx;
        }

        for idx1 in 1..=word1.len() {
            for idx2 in 1..=word2.len() {
                if word1[idx1 - 1] == word2[idx2 - 1] {
                    dp[idx1][idx2] = dp[idx1 - 1][idx2 - 1];
                } else {
                    dp[idx1][idx2] = dp[idx1 - 1][idx2 - 1]
                        .min(dp[idx1 - 1][idx2])
                        .min(dp[idx1][idx2 - 1])
                        + 1;
                }
            }
        }

        return dp[word1.len()][word2.len()] as i32;
    }
}

#[cfg(test)]
mod tests {
    use super::*;
}
