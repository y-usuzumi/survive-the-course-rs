// https://leetcode.com/problems/permutation-sequence/

pub struct Solution;

impl Solution {
    pub fn get_permutation(n: i32, k: i32) -> String {
        fn helper(digits: &mut Vec<char>, k: i32) -> String {
            if k == 0 {
                return digits.iter().collect();
            }

            let perm_n_minus_one: i32 = (1..(digits.len() as i32)).product();
            let digit_idx = (k / perm_n_minus_one) as usize;
            let rem = k % perm_n_minus_one;
            let digit = digits.remove(digit_idx);
            let mut result = String::new();
            result.push(digit);
            result.push_str(&helper(digits, rem));
            return result;
        }

        let s = "123456789";
        let mut digits = s[..n as usize].chars().collect();
        return helper(&mut digits, k - 1);
    }
}

#[cfg(test)]
mod tests {
    use super::*;
}
