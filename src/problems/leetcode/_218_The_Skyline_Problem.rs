// https://leetcode.com/problems/the-skyline-problem/

use std::{
    cmp::Reverse,
    collections::{BTreeMap, BTreeSet, BinaryHeap},
};

pub struct Solution;

impl Solution {
    pub fn get_skyline(buildings: Vec<Vec<i32>>) -> Vec<Vec<i32>> {
        let mut treemap: BTreeMap<i32, i32> = BTreeMap::new();
        let mut heap = BinaryHeap::new();
        for building in buildings {
            heap.push(Reverse((building[0], -building[2])));
            heap.push(Reverse((building[1], building[2])));
        }

        let mut result = vec![];
        while let Some(Reverse((x, y))) = heap.pop() {
            if y < 0 {
                let y = -y;
                // new building
                if treemap.is_empty() {
                    result.push(vec![x, y]);
                } else {
                    let last_entry = treemap.iter().rev().next().unwrap();
                    if y > *last_entry.0 {
                        result.push(vec![x, y]);
                    }
                }
                *treemap.entry(y).or_default() += 1;
            } else if treemap.contains_key(&y) {
                *treemap.entry(y).or_default() -= 1;
                if *treemap.get(&y).unwrap() == 0 {
                    treemap.remove(&y);
                    if treemap.is_empty() {
                        result.push(vec![x, 0]);
                    } else {
                        let last_entry = treemap.iter().rev().next().unwrap();
                        if last_entry.0 < &y {
                            result.push(vec![x, *last_entry.0]);
                        }
                    }
                }
            }
        }
        return result;
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_1() {
        let input = vec![
            vec![2, 9, 10],
            vec![3, 7, 15],
            vec![5, 12, 12],
            vec![15, 20, 10],
            vec![19, 24, 8],
        ];
        let expected = vec![
            vec![2, 10],
            vec![3, 15],
            vec![7, 12],
            vec![12, 0],
            vec![15, 10],
            vec![20, 8],
            vec![24, 0],
        ];
        assert_eq!(Solution::get_skyline(input), expected);
    }
}
