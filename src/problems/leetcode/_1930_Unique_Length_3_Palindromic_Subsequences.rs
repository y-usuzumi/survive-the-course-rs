// https://leetcode.com/problems/unique-length-3-palindromic-subsequences/description/

pub struct Solution;

impl Solution {
    pub fn count_palindromic_subsequence(s: String) -> i32 {
        fn get_pos(ch: char) -> usize {
            return (ch as u8 - 'a' as u8) as usize;
        }
        let mut m = vec![vec![0; 26]; 26];
        let mut last_idxs = vec![usize::MAX; 26];
        let mut twice = vec![false; 26];
        let chars: Vec<char> = s.chars().collect();
        for (idx, ch) in chars.iter().enumerate() {
            let pos = get_pos(*ch);
            let last_idx = last_idxs[pos];
            if last_idx != usize::MAX {
                for mid_ch in &chars[last_idx + 1..idx] {
                    m[pos][get_pos(*mid_ch)] = 1;
                }
                if twice[pos] {
                    m[pos][pos] = 1;
                } else {
                    twice[pos] = true;
                }
            }
            last_idxs[pos] = idx;
        }

        return m.into_iter().map(|v| v.into_iter().sum::<i32>()).sum();
    }
}

#[cfg(test)]
mod tests {
    use super::*;
}
