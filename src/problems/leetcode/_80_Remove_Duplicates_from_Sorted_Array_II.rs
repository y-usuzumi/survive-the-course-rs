// https://leetcode.com/problems/remove-duplicates-from-sorted-array-ii/

pub struct Solution;

const HOLE: i32 = 10000000;

impl Solution {
    // FIXME: This algorithm is stupid! Refer to the solutions!
    pub fn remove_duplicates(nums: &mut Vec<i32>) -> i32 {
        let mut prev = 0;
        let mut appearances = 0;
        for idx in 0..nums.len() {
            if appearances == 0 || nums[idx] != prev {
                prev = nums[idx];
                appearances = 1;
            } else if nums[idx] == prev {
                appearances += 1;
            }
            if appearances > 2 {
                nums[idx] = HOLE;
            }
        }

        let mut idxl = 0;
        for idx in 0..nums.len() {
            if nums[idx] == HOLE {
                continue;
            }
            nums[idxl] = nums[idx];
            idxl += 1;
        }

        return idxl as i32;
    }
}

#[cfg(test)]
mod tests {
    use super::*;
}
