// https://leetcode.com/problems/unique-paths-ii/

pub struct Solution;

impl Solution {
    pub fn unique_paths_with_obstacles(obstacle_grid: Vec<Vec<i32>>) -> i32 {
        let rows = obstacle_grid.len();
        let cols = obstacle_grid[0].len();
        let mut dp = vec![0; cols];
        dp[0] = 1;
        for row_idx in 0..rows {
            for col_idx in 0..cols {
                if obstacle_grid[row_idx][col_idx] == 1 {
                    dp[col_idx] = 0;
                    continue;
                } else if col_idx > 0 {
                    dp[col_idx] += dp[col_idx - 1];
                }
            }
        }
        return dp[cols - 1];
    }
}

#[cfg(test)]
mod tests {
    use super::*;
}
