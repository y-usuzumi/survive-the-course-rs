// https://leetcode.com/problems/check-if-there-is-a-valid-partition-for-the-array/

pub struct Solution;

impl Solution {
    pub fn valid_partition(nums: Vec<i32>) -> bool {
        let mut dp = vec![false; nums.len()];
        fn is_valid_pair(nums: &[i32]) -> bool {
            return nums[0] == nums[1];
        }
        fn is_valid_triplet(nums: &[i32]) -> bool {
            return nums[0] == nums[1] && nums[1] == nums[2]
                || nums[0] + 1 == nums[1] && nums[1] + 1 == nums[2];
        }
        for idx in 1..nums.len() {
            if idx == 1 {
                dp[idx] = is_valid_pair(&nums[idx - 1..=idx]);
            } else if idx == 2 {
                dp[idx] = is_valid_triplet(&nums[idx - 2..=idx]);
            } else {
                dp[idx] = dp[idx - 3] && is_valid_triplet(&nums[idx - 2..=idx])
                    || dp[idx - 2] && is_valid_pair(&nums[idx - 1..=idx]);
            }
        }
        return dp[nums.len() - 1];
    }
}

#[cfg(test)]
mod tests {
    use super::*;
}
