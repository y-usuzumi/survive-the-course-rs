// https://leetcode.com/problems/construct-the-lexicographically-largest-valid-sequence/
use std::{collections::BTreeSet, iter::FromIterator};

pub struct Solution;

impl Solution {
    pub fn construct_distanced_sequence(n: i32) -> Vec<i32> {
        fn backtrack(
            result: &mut Vec<i32>,
            remaining_numbers: &mut BTreeSet<usize>,
            mut curr_pos: usize,
        ) -> bool {
            if remaining_numbers.is_empty() {
                return true;
            }
            if curr_pos == result.len() {
                return false;
            }
            while result[curr_pos] >= 0 && curr_pos < result.len() {
                curr_pos += 1;
            }
            if curr_pos == result.len() {
                return false;
            }
            let numbers = Vec::from_iter(remaining_numbers.iter().rev().map(|n| *n));
            for curr_number in numbers {
                if curr_number > 1
                    && (curr_pos + curr_number >= result.len()
                        || result[curr_pos + curr_number] >= 0)
                {
                    continue;
                }
                remaining_numbers.remove(&curr_number);
                result[curr_pos] = curr_number as i32;
                if curr_number > 1 {
                    result[curr_pos + curr_number] = curr_number as i32;
                }
                if backtrack(result, remaining_numbers, curr_pos + 1) {
                    return true;
                }
                result[curr_pos] = -1;
                if curr_number > 1 {
                    result[curr_pos + curr_number] = -1;
                }
                remaining_numbers.insert(curr_number);
            }

            return false;
        }

        let n = n as usize;
        let mut result = vec![-1; 2 * n - 1];
        let mut remaining_numbers = BTreeSet::from_iter(1..=n);

        backtrack(&mut result, &mut remaining_numbers, 0);
        return result;
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_1() {
        assert_eq!(
            Solution::construct_distanced_sequence(3),
            vec![3, 1, 2, 3, 2]
        );
    }

    #[test]
    fn test_2() {
        assert_eq!(
            Solution::construct_distanced_sequence(5),
            vec![5, 3, 1, 4, 3, 5, 2, 4, 2]
        );
    }
}
