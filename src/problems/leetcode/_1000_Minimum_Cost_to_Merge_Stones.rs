// https://leetcode.com/problems/minimum-cost-to-merge-stones/

pub trait Solution {
    fn merge_stones(stones: Vec<i32>, k: i32) -> i32;
}

// Top-down DP with memo
pub struct Solution1;
// Bottom-up DP
pub struct Solution2;

impl Solution for Solution1 {
    fn merge_stones(stones: Vec<i32>, k: i32) -> i32 {
        let k = k as usize;
        let len = stones.len();
        if (len - 1) % (k - 1) > 0 {
            return -1;
        }

        // Row: left bound
        // Col: right bound
        // Height: Optimal cost to combine [row, col] into `height` piles
        let mut dp = vec![vec![vec![i32::MAX; k + 1]; len]; len];
        // Prefix sum with a prepending 0 (so it's easier to do sums[left - 1])
        // This will simplify the calculation of the sum of a range of piles
        let mut sums = vec![0];
        {
            let mut sum = 0;
            for stone in &stones {
                sum += stone;
                sums.push(sum);
            }
        }
        // Zero cost to merge one stone into one pile.
        for idx in 0..stones.len() {
            dp[idx][idx][1] = 0;
        }

        return Self::helper(&mut dp, &sums, k, 0, len - 1, 1);
    }
}

impl Solution1 {
    fn helper(
        dp: &mut Vec<Vec<Vec<i32>>>,
        sums: &Vec<i32>,
        k: usize,
        left: usize,
        right: usize,
        piles: usize,
    ) -> i32 {
        if dp[left][right][piles] != i32::MAX {
            return dp[left][right][piles];
        }

        // With every merge we have k-1 fewer piles. At last we need to have
        // `piles` piles remaining. For example, if we start
        // with 4 and we merge 3 stones, we will end up with 2 piles.

        // stones = merges * (k - 1) + piles => merges = (stones - piles) / (k - 1) ∈ N+
        if (right - left + 1 - piles) % (k - 1) > 0 {
            dp[left][right][piles] = -1;
            return -1;
        }

        if piles == 1 {
            // Unless it is a single-stone pile, we need to have `k` piles so we
            // can merge them into one.
            let result_k = Self::helper(dp, sums, k, left, right, k);
            let result = if result_k == -1 {
                -1
            } else {
                // The additional cost of merging k piles is the sum of all
                // stones
                result_k + sums[right + 1] - sums[left]
            };
            dp[left][right][piles] = result;
            return result;
        }

        // In any other case, we can split the piles into two and divide the problem:
        // 1. Find the minimum cost of merging the left half into one pile
        // 2. Find the minimum cost of merging the right half into k-1 piles.
        let mut min_cost = i32::MAX;
        for idx in left..right {
            // Subproblem 1
            let cost_left = Self::helper(dp, sums, k, left, idx, 1);
            if cost_left == -1 {
                continue;
            }
            // Subproblem 2
            let cost_right = Self::helper(dp, sums, k, idx + 1, right, piles - 1);
            if cost_right == -1 {
                continue;
            }
            if cost_left + cost_right < min_cost {
                min_cost = cost_left + cost_right;
            }
        }
        let result = if min_cost == i32::MAX { -1 } else { min_cost };
        dp[left][right][piles] = result;
        return result;
    }
}

impl Solution for Solution2 {
    // Bottom up DP
    fn merge_stones(stones: Vec<i32>, k: i32) -> i32 {
        todo!();
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_1() {
        assert_eq!(Solution1::merge_stones(vec![3, 2, 4, 1], 2), 20);
    }

    #[test]
    fn test_2() {
        assert_eq!(Solution1::merge_stones(vec![3, 2, 4, 1], 3), -1);
    }

    #[test]
    fn test_3() {
        assert_eq!(Solution1::merge_stones(vec![3, 5, 1, 2, 6], 3), 25);
    }
}
