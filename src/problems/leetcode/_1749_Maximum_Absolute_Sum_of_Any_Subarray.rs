// https://leetcode.com/problems/maximum-absolute-sum-of-any-subarray/

pub trait Solution {
    fn max_absolute_sum(nums: Vec<i32>) -> i32;
}

pub struct Solution1;

impl Solution for Solution1 {
    // 思路是把前缀和想象成一个曲线，其最高的波峰和最低的波谷的差的绝对值即为结果，而且
    // 由于是取绝对值，甚至与谁在前谁在后无关。
    fn max_absolute_sum(nums: Vec<i32>) -> i32 {
        let mut top_sum = 0;
        let mut bottom_sum = 0;
        let mut curr_sum = 0;
        for idx in 0..nums.len() {
            curr_sum += nums[idx];
            top_sum = top_sum.max(curr_sum);
            bottom_sum = bottom_sum.min(curr_sum);
        }
        return (top_sum - bottom_sum).abs();
    }
}

// 本题的第二种解法类似于最大子数组和，只不过需要同时求最大和最小，取两者绝对值较大者。
pub struct Solution2;

#[cfg(test)]
mod tests {
    use super::*;
}
