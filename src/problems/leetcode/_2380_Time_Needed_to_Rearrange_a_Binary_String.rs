// https://leetcode.com/problems/time-needed-to-rearrange-a-binary-string/

pub struct Solution;

impl Solution {
    pub fn seconds_to_remove_occurrences(s: String) -> i32 {
        let mut ones = 0;
        let mut result = 0;
        for (idx, ch) in s.chars().enumerate() {
            match ch {
                '0' => {
                    continue;
                }
                '1' => {
                    if idx > ones {
                        result = (idx - ones).max(result + 1);
                    }
                    ones += 1;
                }
                _ => continue,
            }
        }
        return result as i32;
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_1() {
        assert_eq!(
            Solution::seconds_to_remove_occurrences("0110101".to_string()),
            4
        );
    }

    #[test]
    fn test_2() {
        assert_eq!(
            Solution::seconds_to_remove_occurrences("11100".to_string()),
            0
        );
    }

    #[test]
    fn test_3() {
        assert_eq!(
            Solution::seconds_to_remove_occurrences("001011".to_string()),
            4
        );
    }

    #[test]
    fn test_4() {
        assert_eq!(
            Solution::seconds_to_remove_occurrences("1011010101".to_string()),
            5
        );
    }

    #[test]
    fn test_5() {
        assert_eq!(
            Solution::seconds_to_remove_occurrences(
                "1001111111110001011001110000000110101".to_string()
            ),
            20
        );
    }
}
