// https://leetcode.com/problems/n-queens-ii/

pub struct Solution;

impl Solution {
    pub fn total_n_queens(n: i32) -> i32 {
        let n = n as usize;
        let mut occupied_cols = vec![false; n];
        let mut occupied_diags_p = vec![false; n * 2 - 1];
        let mut occupied_diags_n = vec![false; n * 2 - 1];
        fn n_queens_helper(
            n: usize,
            remaining_queens: usize,
            occupied_cols: &mut Vec<bool>,
            occupied_diags_p: &mut Vec<bool>,
            occupied_diags_n: &mut Vec<bool>,
        ) -> i32 {
            if remaining_queens == 0 {
                return 1;
            }
            let mut result = 0;
            let row_idx = n - remaining_queens;
            for col_idx in 0..n {
                if occupied_cols[col_idx] {
                    continue;
                }
                let diag_p_idx = n + col_idx - row_idx - 1;
                if occupied_diags_p[diag_p_idx] {
                    continue;
                }

                let diag_n_idx = col_idx + row_idx;
                if occupied_diags_n[diag_n_idx] {
                    continue;
                }
                occupied_cols[col_idx] = true;
                occupied_diags_p[diag_p_idx] = true;
                occupied_diags_n[diag_n_idx] = true;
                result += n_queens_helper(
                    n,
                    remaining_queens - 1,
                    occupied_cols,
                    occupied_diags_p,
                    occupied_diags_n,
                );
                occupied_cols[col_idx] = false;
                occupied_diags_p[diag_p_idx] = false;
                occupied_diags_n[diag_n_idx] = false;
            }
            return result;
        }
        return n_queens_helper(
            n as usize,
            n,
            &mut occupied_cols,
            &mut occupied_diags_p,
            &mut occupied_diags_n,
        );
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_1() {
        assert_eq!(Solution::total_n_queens(4), 2);
    }
}
