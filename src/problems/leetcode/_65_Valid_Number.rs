// https://leetcode.com/problems/valid-number/description/

pub struct Solution;

impl Solution {
    pub fn is_number(s: String) -> bool {
        fn read_char(chars: &[char], pos: usize) -> Option<(char, usize)> {
            if pos >= chars.len() {
                return None;
            }
            return Some((chars[pos], pos + 1));
        }

        fn parse_one_or_more_digits(chars: &[char], mut pos: usize) -> Option<usize> {
            let mut is_valid = false;
            while let Some((ch, new_pos)) = read_char(chars, pos) {
                if (ch as u8) < ('0' as u8) || (ch as u8) > ('9' as u8) {
                    break;
                }
                is_valid = true;
                pos = new_pos;
            }

            return if is_valid { Some(pos) } else { None };
        }

        fn parse_sign(chars: &[char], pos: usize) -> Option<usize> {
            let (ch, new_pos) = read_char(chars, pos)?;
            if ch == '+' || ch == '-' {
                return Some(new_pos);
            }
            return None;
        }

        fn parse_integer(chars: &[char], mut pos: usize) -> Option<usize> {
            if pos >= chars.len() {
                return None;
            }

            if let Some(new_pos) = parse_sign(chars, pos) {
                pos = new_pos;
            }

            return parse_one_or_more_digits(chars, pos);
        }

        fn parse_decimal_or_integer(chars: &[char], mut pos: usize) -> Option<usize> {
            if let Some(new_pos) = parse_integer(chars, pos) {
                if let Some((ch, new_pos2)) = read_char(chars, new_pos) {
                    if ch == '.' {
                        if let Some(new_pos3) = parse_one_or_more_digits(chars, new_pos2) {
                            return Some(new_pos3);
                        }
                        return Some(new_pos2);
                    }
                }
                return Some(new_pos);
            } else {
                if let Some(new_pos) = parse_sign(chars, pos) {
                    pos = new_pos;
                }
                if let Some((ch, new_pos)) = read_char(chars, pos) {
                    if ch == '.' {
                        if let Some(new_pos2) = parse_one_or_more_digits(chars, new_pos) {
                            return Some(new_pos2);
                        }
                    }
                }
                return None;
            }
        }

        let chars: Vec<char> = s.chars().collect();
        let new_pos = parse_decimal_or_integer(&chars, 0);
        if new_pos.is_none() {
            return false;
        }

        let pos = new_pos.unwrap();

        if let Some((ch, new_pos)) = read_char(&chars, pos) {
            if ch == 'e' || ch == 'E' {
                if let Some(new_pos) = parse_integer(&chars, new_pos) {
                    return new_pos >= chars.len();
                }
                return false;
            }
            return false;
        }

        return true;
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_1() {
        assert!(!Solution::is_number("e".to_string()));
    }

    #[test]
    fn test_2() {
        assert!(Solution::is_number("+.8".to_string()));
    }
}
