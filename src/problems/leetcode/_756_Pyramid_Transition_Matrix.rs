// https://leetcode.com/problems/pyramid-transition-matrix/

use std::collections::HashMap;

pub struct Solution;

impl Solution {
    pub fn pyramid_transition(bottom: String, allowed: Vec<String>) -> bool {
        fn char_pair_to_u16(ch1: char, ch2: char) -> u16 {
            return (ch1 as u16) << 8 | ch2 as u16;
        }

        let mut lookup: HashMap<u16, Vec<char>> = HashMap::new();
        for s in allowed {
            let chars: Vec<char> = s.chars().collect();
            lookup
                .entry(char_pair_to_u16(chars[0], chars[1]))
                .or_default()
                .push(chars[2]);
        }
        fn dfs(chars: &Vec<char>, lookup: &HashMap<u16, Vec<char>>) -> bool {
            if chars.len() == 1 {
                return true;
            }
            let mut permutations = vec![];
            for idx in 0..chars.len() - 1 {
                if let Some(options) = lookup.get(&char_pair_to_u16(chars[idx], chars[idx + 1])) {
                    permutations.push(options.clone());
                } else {
                    return false;
                }
            }
            let mut stack = vec![];
            return backtrack(&mut stack, lookup, &permutations);
        }

        fn backtrack(
            stack: &mut Vec<char>,
            lookup: &HashMap<u16, Vec<char>>,
            remaining_permutations: &[Vec<char>],
        ) -> bool {
            if remaining_permutations.is_empty() {
                return dfs(stack, lookup);
            }
            for &ch in &remaining_permutations[0] {
                stack.push(ch);
                if backtrack(stack, lookup, &remaining_permutations[1..]) {
                    return true;
                }
                stack.pop();
            }
            return false;
        }

        return dfs(&bottom.chars().collect(), &lookup);
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_1() {
        assert!(Solution::pyramid_transition(
            "BCD".to_string(),
            vec![
                "BCC".to_string(),
                "CDE".to_string(),
                "CEA".to_string(),
                "FFF".to_string()
            ]
        ));
    }

    #[test]
    fn test_2() {
        assert!(!Solution::pyramid_transition(
            "AAAA".to_string(),
            vec![
                "AAB".to_string(),
                "AAC".to_string(),
                "BCD".to_string(),
                "BBE".to_string(),
                "DEF".to_string()
            ]
        ));
    }
}
