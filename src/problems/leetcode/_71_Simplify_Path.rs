// https://leetcode.com/problems/simplify-path/

pub struct Solution;

impl Solution {
    pub fn simplify_path(path: String) -> String {
        let segments: Vec<_> = path.split('/').collect();
        let mut path_stack = vec![];
        for seg in segments {
            if seg.is_empty() || seg == "." {
                continue;
            }
            if seg == ".." {
                path_stack.pop();
                continue;
            }
            path_stack.push(seg);
        }
        let mut result = String::new();
        result.push('/');
        result.push_str(&path_stack.join("/"));
        return result;
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_1() {
        assert_eq!(
            Solution::simplify_path("/home/".to_string()),
            "/home".to_string()
        );
    }

    #[test]
    fn test_2() {
        assert_eq!(Solution::simplify_path("/../".to_string()), "/".to_string());
    }

    #[test]
    fn test_3() {
        assert_eq!(
            Solution::simplify_path("/home//foo/".to_string()),
            "/home/foo".to_string()
        );
    }
}
