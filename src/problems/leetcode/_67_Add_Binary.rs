// https://leetcode.com/problems/add-binary/

pub struct Solution;

impl Solution {
    pub fn add_binary(a: String, b: String) -> String {
        if a.len() < b.len() {
            return Self::add_binary(b, a);
        }
        let chars_a: Vec<char> = a.chars().rev().collect();
        let chars_b: Vec<char> = b.chars().rev().collect();
        let mut result = vec![];
        let mut carry = 0;
        let mut idx = 0;
        while idx < chars_a.len() {
            let bit_a = chars_a[idx] as u8 - '0' as u8;
            let bit_b = if idx >= chars_b.len() {
                0
            } else {
                chars_b[idx] as u8 - '0' as u8
            };
            let sum = bit_a + bit_b + carry;
            if sum >= 2 {
                carry = 1;
                result.push(('0' as u8 + sum - 2) as char);
            } else {
                carry = 0;
                result.push(('0' as u8 + sum) as char);
            }
            idx += 1;
        }
        if carry == 1 {
            result.push('1');
        }
        return result.into_iter().rev().collect();
    }
}

#[cfg(test)]
mod tests {
    use super::*;
}
