// https://leetcode.com/problems/find-the-city-with-the-smallest-number-of-neighbors-at-a-threshold-distance/

pub struct Solution;

impl Solution {
    pub fn find_the_city(n: i32, edges: Vec<Vec<i32>>, distance_threshold: i32) -> i32 {
        let n = n as usize;
        const MAX: i32 = 10_000_000;
        let mut distances = vec![vec![MAX; n]; n];
        for i in 0..n {
            distances[i][i] = 0;
        }
        for edge in edges {
            distances[edge[0] as usize][edge[1] as usize] = edge[2];
            distances[edge[1] as usize][edge[0] as usize] = edge[2];
        }
        for k in 0..n {
            for i in 0..n {
                for j in i..n {
                    if distances[i][j] > distances[i][k] + distances[k][j] {
                        distances[i][j] = distances[i][k] + distances[k][j];
                        distances[j][i] = distances[i][k] + distances[k][j];
                    }
                }
            }
        }
        let mut node = 0;
        let mut node_neighbors = i32::MAX;
        for i in 0..n {
            let mut neighbors = 0;
            for j in 0..n {
                if i == j || distances[i][j] > distance_threshold {
                    continue;
                }
                neighbors += 1;
            }
            if neighbors <= node_neighbors {
                node = i;
                node_neighbors = neighbors;
            }
        }
        return node as i32;
    }
}

#[cfg(test)]
mod tests {
    use super::*;
}
