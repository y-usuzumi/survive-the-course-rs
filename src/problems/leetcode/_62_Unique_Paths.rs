// https://leetcode.com/problems/unique-paths/

pub struct Solution;

impl Solution {
    pub fn unique_paths(m: i32, n: i32) -> i32 {
        let m = m as usize;
        let n = n as usize;
        let mut dp = vec![0; m];
        dp[0] = 1;
        for row in 0..n {
            for idx in 1..m {
                dp[idx] += dp[idx - 1];
            }
        }

        return dp[dp.len() - 1];
    }
}

#[cfg(test)]
mod tests {
    use super::*;
}
