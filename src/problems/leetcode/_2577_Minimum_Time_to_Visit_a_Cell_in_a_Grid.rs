// https://leetcode.com/problems/minimum-time-to-visit-a-cell-in-a-grid/

use std::{
    cmp::Reverse,
    collections::{BinaryHeap, HashSet},
};

#[derive(PartialOrd, Ord, PartialEq, Eq)]
pub struct PosAndTime(i32, usize, usize);
pub struct Solution;

impl Solution {
    // 我是傻逼
    pub fn minimum_time(grid: Vec<Vec<i32>>) -> i32 {
        let mut h = BinaryHeap::new();
        let mut visited = HashSet::new();
        if grid.len() > 1 && grid[0].len() > 1 {
            if grid[0][1] > 1 && grid[1][0] > 1 {
                return -1;
            }
        }
        h.push(Reverse(PosAndTime(grid[0][0], 0, 0)));
        fn min_next_time(curr_time: i32, mut cell_time: i32) -> i32 {
            if cell_time <= curr_time {
                cell_time = curr_time;
            }
            return cell_time
                + if (cell_time - curr_time) % 2 == 0 {
                    1
                } else {
                    0
                };
        }
        while let Some(Reverse(PosAndTime(time, row_idx, col_idx))) = h.pop() {
            if row_idx == grid.len() - 1 && col_idx == grid[0].len() - 1 {
                return time;
            }
            if visited.contains(&(row_idx, col_idx)) {
                continue;
            }
            visited.insert((row_idx, col_idx));
            if row_idx < grid.len() - 1 {
                h.push(Reverse(PosAndTime(
                    min_next_time(time, grid[row_idx + 1][col_idx]),
                    row_idx + 1,
                    col_idx,
                )));
            }

            if row_idx > 0 {
                h.push(Reverse(PosAndTime(
                    min_next_time(time, grid[row_idx - 1][col_idx]),
                    row_idx - 1,
                    col_idx,
                )));
            }

            if col_idx < grid[0].len() - 1 {
                h.push(Reverse(PosAndTime(
                    min_next_time(time, grid[row_idx][col_idx + 1]),
                    row_idx,
                    col_idx + 1,
                )));
            }

            if col_idx > 0 {
                h.push(Reverse(PosAndTime(
                    min_next_time(time, grid[row_idx][col_idx - 1]),
                    row_idx,
                    col_idx - 1,
                )));
            }
        }

        return -1;
    }
}

#[cfg(test)]
mod tests {
    use super::*;
}
