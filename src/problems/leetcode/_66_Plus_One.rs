// https://leetcode.com/problems/plus-one/

pub struct Solution;

impl Solution {
    pub fn plus_one(mut digits: Vec<i32>) -> Vec<i32> {
        for idx in (0..digits.len()).rev() {
            let digit = digits[idx];
            if digit == 9 {
                digits[idx] = 0;
            } else {
                digits[idx] += 1;
                return digits;
            }
        }
        digits.insert(0, 1);
        return digits;
    }
}

#[cfg(test)]
mod tests {
    use super::*;
}
