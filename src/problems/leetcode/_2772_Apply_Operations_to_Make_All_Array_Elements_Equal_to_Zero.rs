// https://leetcode.com/problems/apply-operations-to-make-all-array-elements-equal-to-zero/

pub trait Solution {
    fn check_array(nums: Vec<i32>, k: i32) -> bool;
}

pub struct Solution1;

impl Solution for Solution1 {
    // When we subtract n from a subarray of size k, we do not apply subtraction to every
    // element in the subarray. Instead, we keep a variable curr_diff representing how much
    // we should subtract, and apply it as we iterate over each element. At the same time,
    // we also maintain a new array resume_diffs which contains all n's that should be added
    // back at the next element following the k-size subarray.
    fn check_array(nums: Vec<i32>, k: i32) -> bool {
        let k = k as usize;
        let mut curr_diff = 0;
        let mut resume_diffs = vec![0; nums.len()];
        for (idx, &(mut num)) in nums.iter().enumerate() {
            curr_diff -= resume_diffs[idx];
            num -= curr_diff;
            if num == 0 {
                continue;
            }
            if num < 0 || num > 0 && idx > nums.len() - k {
                return false;
            }
            curr_diff += num;
            resume_diffs[idx + k] = num;
        }

        return true;
    }
}

pub struct Solution2;

impl Solution for Solution2 {
    fn check_array(nums: Vec<i32>, k: i32) -> bool {
        todo!("Use in-place");
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_1() {
        assert!(Solution1::check_array(vec![4, 4, 4, 5, 5, 5], 3));
    }

    #[test]
    fn test_2() {
        assert!(Solution1::check_array(vec![4, 4, 5, 1, 1], 3));
    }

    #[test]
    fn test_3() {
        assert!(Solution1::check_array(vec![4, 5, 6, 2, 1], 3));
    }
}
