// https://leetcode.com/problems/sort-colors/

pub struct Solution;

impl Solution {
    pub fn sort_colors(nums: &mut Vec<i32>) {
        let mut idxl = 0;
        let mut idxr = nums.len() - 1;
        let mut idx = 0;
        while idx <= idxr {
            if nums[idx] == 0 {
                nums[idxl] = 0;
                idxl += 1;
            } else if nums[idx] == 2 {
                nums.swap(idx, idxr);
                // If nums contains only 2, idxr will eventually become -1 (underflow) and cause index-out-of-bound error.
                if idxr == 0 {
                    return;
                }
                idxr -= 1;
                continue;
            }
            idx += 1;
        }
        for idx in idxl..=idxr {
            nums[idx] = 1;
        }
        return;
    }
}

#[cfg(test)]
mod tests {
    use super::*;
}
