// https://leetcode.com/problems/minimum-number-of-swaps-to-make-the-string-balanced/description/

pub struct Solution;

impl Solution {
    pub fn min_swaps(s: String) -> i32 {
        let mut w = 0;
        let mut max = 0;
        for ch in s.chars() {
            if ch == ']' {
                w += 1;
            } else {
                w -= 1;
            }
            max = max.max(w);
        }

        return (max + 1) / 2;
    }
}

#[cfg(test)]
mod tests {
    use super::*;
}
