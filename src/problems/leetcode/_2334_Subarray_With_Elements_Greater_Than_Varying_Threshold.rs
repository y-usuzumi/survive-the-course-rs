// https://leetcode.com/problems/subarray-with-elements-greater-than-varying-threshold/

pub struct Solution;

impl Solution {
    pub fn valid_subarray_size(nums: Vec<i32>, threshold: i32) -> i32 {
        let min_sizes: Vec<_> = nums.iter().map(|n| (threshold / n) + 1).collect();
        println!("{:?}", min_sizes);
        let mut stack: Vec<(i32, i32)> = Vec::new();
        for (new_idx, &new_size) in min_sizes.iter().enumerate() {
            let new_idx = new_idx as i32;
            let mut prev_idx = new_idx;
            while let Some(&(curr_idx, curr_size)) = stack.last() {
                if new_size > curr_size {
                    stack.pop();
                    prev_idx = curr_idx;
                } else if curr_size <= new_idx - curr_idx + 1 {
                    println!("Current stack: {:?}", stack);
                    println!("new_idx: {}, new_size: {}", new_idx, new_size);
                    return curr_size;
                } else {
                    break;
                }
            }
            if let Some(&(curr_idx, curr_size)) = stack.last() {
                if new_size < curr_size - prev_idx + curr_idx {
                    stack.push((prev_idx, new_size));
                }
            } else {
                if new_size == 1 {
                    return 1;
                }
                stack.push((prev_idx, new_size));
            }
        }
        return -1;
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_1() {
        assert_eq!(Solution::valid_subarray_size(vec![1, 3, 4, 3, 1], 6), 3);
    }

    #[test]
    fn test_2() {
        assert_eq!(Solution::valid_subarray_size(vec![6, 5, 6, 5, 8], 7), 2);
    }

    #[test]
    fn test_3() {
        assert_eq!(Solution::valid_subarray_size(vec![8], 7), 1);
    }

    #[test]
    fn test_4() {
        assert_eq!(
            Solution::valid_subarray_size(
                vec![818, 232, 595, 418, 608, 229, 37, 330, 876, 774, 931, 939, 479, 884, 354, 328],
                3790
            ),
            -1
        );
    }
}
