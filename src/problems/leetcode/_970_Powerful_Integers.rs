// https://leetcode.com/problems/powerful-integers/description/

use std::collections::HashSet;

pub struct Solution;

impl Solution {
    pub fn powerful_integers(x: i32, y: i32, bound: i32) -> Vec<i32> {
        let mut result = HashSet::new();
        let mut xpowered = 1;
        for _ in 0..(if x == 1 { 1 } else { 10000 }) {
            let mut ypowered = 1;
            for _ in 0..(if y == 1 { 1 } else { 10000 }) {
                if xpowered + ypowered < bound {
                    result.insert(xpowered + ypowered);
                } else {
                    break;
                }
                ypowered *= y;
            }
            xpowered *= x;
            if xpowered >= bound {
                break;
            }
        }
        return result.into_iter().collect();
    }
}

#[cfg(test)]
mod tests {
    use super::*;
}
