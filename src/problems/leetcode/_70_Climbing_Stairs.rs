// https://leetcode.com/problems/climbing-stairs/

pub struct Solution;

impl Solution {
    pub fn climb_stairs(n: i32) -> i32 {
        if n == 1 {
            return 1;
        }
        if n == 2 {
            return 2;
        }
        let mut curr = 3;
        let mut minus_2 = 1;
        let mut minus_1 = 2;
        let mut result = 0;
        while curr <= n {
            result = minus_2 + minus_1;
            minus_2 = minus_1;
            minus_1 = result;
            curr += 1;
        }
        return result;
    }
}

#[cfg(test)]
mod tests {
    use super::*;
}
