// https://leetcode.com/problems/integer-to-english-words/

pub struct Solution;

const atomic_words: &[&str] = &[
    "Zero",
    "One",
    "Two",
    "Three",
    "Four",
    "Five",
    "Six",
    "Seven",
    "Eight",
    "Nine",
    "Ten",
    "Eleven",
    "Twelve",
    "Thirteen",
    "Fourteen",
    "Fifteen",
    "Sixteen",
    "Seventeen",
    "Eighteen",
    "Nineteen",
    "Twenty",
];
const tens_words: &[&str] = &[
    "Zero", "Ten", "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety",
];
const thousand_exps: &[&str] = &["", "Thousand", "Million", "Billion"];
const hundred: &str = "Hundred";
const and: &str = "And";

impl Solution {
    pub fn number_to_words(mut num: i32) -> String {
        fn section_to_words(num: i32) -> Vec<String> {
            fn two_digits_to_words(num: i32) -> Vec<String> {
                let mut result = vec![];
                if num < 20 {
                    result.push(atomic_words[num as usize].to_string());
                } else {
                    result.push(tens_words[(num / 10) as usize].to_string());
                    if num % 10 > 0 {
                        result.push(atomic_words[(num % 10) as usize].to_string());
                    }
                }
                return result;
            }
            let mut result = vec![];
            if num >= 100 {
                result.push(atomic_words[(num / 100) as usize].to_string());
                result.push(hundred.to_string());
            }
            if num % 100 > 0 {
                result.extend(two_digits_to_words(num % 100));
            }

            return result;
        }
        if num == 0 {
            return atomic_words[num as usize].to_string();
        }
        let mut result = vec![];
        let mut thousand_exp_idx = 0;
        while num > 0 {
            let section = num % 1000;
            if section > 0 {
                if thousand_exp_idx > 0 {
                    result.insert(0, vec![thousand_exps[thousand_exp_idx].to_string()]);
                }
                result.insert(0, section_to_words(section));
            }

            num = num / 1000;
            thousand_exp_idx += 1;
        }

        return result.concat().join(" ");
    }
}

#[cfg(test)]
mod tests {
    use super::*;
}
