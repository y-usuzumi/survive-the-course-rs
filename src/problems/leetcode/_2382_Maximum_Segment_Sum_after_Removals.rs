// https://leetcode.com/problems/maximum-segment-sum-after-removals/

use std::collections::BTreeMap;

pub struct Solution;

trait BTreeMapExt<K, V> {
    fn lower(&self, k: &K) -> Option<(&K, &V)>;
    fn upper(&self, k: &K) -> Option<(&K, &V)>;
}

impl<V> BTreeMapExt<usize, V> for BTreeMap<usize, V> {
    fn lower(&self, k: &usize) -> Option<(&usize, &V)> {
        self.range(0..*k).rev().next()
    }

    fn upper(&self, k: &usize) -> Option<(&usize, &V)> {
        self.range(*k + 1..=usize::MAX).next()
    }
}

impl Solution {
    pub fn maximum_segment_sum(nums: Vec<i32>, remove_queries: Vec<i32>) -> Vec<i64> {
        let mut treemap: BTreeMap<usize, (usize, i64)> = BTreeMap::new();
        let queries = remove_queries
            .into_iter()
            .rev()
            .map(|n| n as usize)
            .collect::<Vec<_>>();
        let mut max_sum = 0;
        let mut result = Vec::with_capacity(nums.len());
        for query in queries {
            result.push(max_sum);
            let left = treemap
                .lower(&query)
                .map(|(&start, &(end, sum))| (start, (end, sum)));
            let right = treemap
                .upper(&query)
                .map(|(&start, &(end, sum))| (start, (end, sum)));
            let mut merge_left = false;
            let mut merge_right = false;
            let mut new_sum = nums[query] as i64;
            let mut left_start: usize = 0;
            let mut right_start: usize = 0;
            let mut right_end: usize = 0;
            if let Some((start, (end, sum))) = left {
                if end == query - 1 {
                    merge_left = true;
                    new_sum += sum;
                    left_start = start;
                }
            }
            if let Some((start, (end, sum))) = right {
                if start == query + 1 {
                    merge_right = true;
                    new_sum += sum;
                    right_start = start;
                    right_end = end;
                }
            }
            if merge_left && merge_right {
                treemap.remove(&right_start);
                treemap.insert(left_start, (right_end, new_sum));
            } else if merge_left {
                treemap.insert(left_start, (query, new_sum));
            } else if merge_right {
                treemap.remove(&right_start);
                treemap.insert(query, (right_end, new_sum));
            } else {
                treemap.insert(query, (query, new_sum));
            }
            max_sum = max_sum.max(new_sum);
        }

        return result.into_iter().rev().collect();
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_1() {
        assert_eq!(
            Solution::maximum_segment_sum(vec![1, 2, 5, 6, 1], vec![0, 3, 2, 4, 1]),
            vec![14, 7, 2, 2, 0]
        );
    }

    #[test]
    fn test_2() {
        assert_eq!(
            Solution::maximum_segment_sum(vec![3, 2, 11, 1], vec![3, 2, 1, 0]),
            vec![16, 5, 3, 0]
        );
    }
}
