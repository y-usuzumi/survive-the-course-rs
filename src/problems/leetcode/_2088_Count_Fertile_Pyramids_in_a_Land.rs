// https://leetcode.com/problems/count-fertile-pyramids-in-a-land/

pub struct Solution;

impl Solution {
    pub fn count_pyramids(mut grid: Vec<Vec<i32>>) -> i32 {
        fn dp(
            grid: &Vec<Vec<i32>>,
            heights: &mut Vec<Vec<i32>>,
            row_idx: usize,
            col_idx: usize,
        ) -> i32 {
            if heights[row_idx][col_idx] >= 0 {
                return heights[row_idx][col_idx];
            }
            let rows = grid.len();
            let cols = grid[0].len();
            let result;
            if grid[row_idx][col_idx] == 0 {
                result = 0;
            } else if col_idx == 0 || col_idx == cols - 1 || row_idx == rows - 1 {
                result = 1;
            } else {
                result = dp(grid, heights, row_idx + 1, col_idx - 1)
                    .min(dp(grid, heights, row_idx + 1, col_idx))
                    .min(dp(grid, heights, row_idx + 1, col_idx + 1))
                    + 1;
            }
            heights[row_idx][col_idx] = result;
            return result;
        }

        let mut result = 0;

        let rows = grid.len();
        let cols = grid[0].len();

        // ▲
        let mut heights = vec![vec![-1; cols]; rows];
        for row_idx in 0..rows {
            for col_idx in 0..cols {
                dp(&grid, &mut heights, row_idx, col_idx);
            }
        }

        for row_idx in 0..rows {
            for col_idx in 0..cols {
                result += (heights[row_idx][col_idx] - 1).max(0);
            }
        }

        // ▾
        for row_idx in 0..(rows / 2) {
            grid.swap(row_idx, rows - row_idx - 1);
        }
        let mut heights = vec![vec![-1; cols]; rows];
        for row_idx in 0..rows {
            for col_idx in 0..cols {
                dp(&grid, &mut heights, row_idx, col_idx);
            }
        }

        for row_idx in 0..rows {
            for col_idx in 0..cols {
                result += (heights[row_idx][col_idx] - 1).max(0);
            }
        }

        return result;
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_1() {
        let grid = vec![vec![0, 1, 1, 0], vec![1, 1, 1, 1]];
        assert_eq!(Solution::count_pyramids(grid), 2);
    }

    #[test]
    fn test_2() {
        let grid = vec![vec![1, 1, 1], vec![1, 1, 1]];
        assert_eq!(Solution::count_pyramids(grid), 2);
    }

    #[test]
    fn test_3() {
        let grid = vec![
            vec![1, 1, 1, 1, 0],
            vec![1, 1, 1, 1, 1],
            vec![1, 1, 1, 1, 1],
            vec![0, 1, 0, 0, 1],
        ];
        assert_eq!(Solution::count_pyramids(grid), 13);
    }
}
