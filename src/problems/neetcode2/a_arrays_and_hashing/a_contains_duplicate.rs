// https://leetcode.cn/problems/contains-duplicate/solutions/518991/cun-zai-zhong-fu-yuan-su-by-leetcode-sol-iedd/

use std::collections::HashSet;

pub struct Solution;

impl Solution {
    pub fn contains_duplicate(nums: Vec<i32>) -> bool {
        let mut hs = HashSet::new();
        for num in nums {
            if hs.contains(&num) {
                return true;
            }
            hs.insert(num);
        }
        return false;
    }
}

#[cfg(test)]
mod tests {
    use super::*;
}
