// https://leetcode.cn/problems/alien-dictionary/

use std::collections::{HashMap, HashSet};

type Graph = HashMap<char, HashSet<char>>;
type Prereq = HashMap<char, usize>;
pub struct Solution;

impl Solution {
    pub fn alien_order(words: Vec<String>) -> String {
        let mut g = HashMap::new();
        let mut prereq = HashMap::new();
        let word_chars: Vec<Vec<char>> = words.into_iter().map(|s| s.chars().collect()).collect();
        for word in &word_chars {
            for c in word {
                g.entry(*c).or_insert(HashSet::new());
                prereq.entry(*c).or_insert(0);
            }
        }

        if !Self::populate_graph(&mut g, &mut prereq, &word_chars, 0) {
            return "".to_string();
        }

        let mut v: Vec<char> = prereq
            .iter()
            .filter(|(&_, &v)| v == 0)
            .map(|(k, _)| *k)
            .collect();
        let mut result: Vec<char> = Vec::new();

        while !v.is_empty() {
            let mut next = vec![];
            for ch in v {
                result.push(ch);
                for next_ch in g.get(&ch).unwrap() {
                    let v = prereq.get_mut(next_ch).unwrap();
                    *v -= 1;
                    if *v == 0 {
                        next.push(*next_ch);
                    }
                }
            }
            v = next;
        }

        return if result.len() == prereq.len() {
            result.into_iter().collect()
        } else {
            "".to_string()
        };
    }

    fn populate_graph(
        g: &mut Graph,
        prereq: &mut Prereq,
        words: &Vec<Vec<char>>,
        idx: usize,
    ) -> bool {
        if words.is_empty() {
            return true;
        }

        let mut groups = vec![];
        let mut prev_char = words[0][idx];
        let mut curr_group: Vec<Vec<char>> = vec![];
        for word in words {
            let curr_char = word[idx];
            if curr_char == prev_char {
                if word.len() > idx + 1 {
                    curr_group.push(word.clone())
                } else if curr_group.len() > 0 {
                    return false;
                }
            } else {
                let set = g.get_mut(&prev_char).unwrap();
                if !set.contains(&curr_char) {
                    set.insert(curr_char);
                    *prereq.get_mut(&curr_char).unwrap() += 1;
                }
                prev_char = curr_char;
                groups.push(curr_group);
                curr_group = vec![];
                if word.len() > idx + 1 {
                    curr_group.push(word.clone());
                }
            }
        }
        if !curr_group.is_empty() {
            groups.push(curr_group);
        }

        for group in groups {
            if !Self::populate_graph(g, prereq, &group, idx + 1) {
                return false;
            }
        }
        return true;
    }
}

#[cfg(test)]
mod tests {
    use test_util::strs_into_strings;

    use super::*;

    #[test]
    fn test_1() {
        let words = strs_into_strings(vec!["wrt", "wrf", "er", "ett", "rftt"]);
        assert_eq!(Solution::alien_order(words), "wertf");
    }

    #[test]
    fn test_2() {
        let words = strs_into_strings(vec!["z", "x"]);
        assert_eq!(Solution::alien_order(words), "zx");
    }

    #[test]
    fn test_3() {
        let words = strs_into_strings(vec!["ac", "ab", "zc", "zb"]);
        assert_eq!(Solution::alien_order(words), "aczb".to_string());
    }

    #[test]
    fn test_4() {
        let words = strs_into_strings(vec!["abc", "ab"]);
        assert_eq!(Solution::alien_order(words), "");
    }

    #[test]
    fn test_5() {
        let words = strs_into_strings(vec!["z", "z"]);
        assert_eq!(Solution::alien_order(words), "z");
    }

    #[test]
    fn test_6() {
        let words = strs_into_strings(vec!["z", "x", "a", "zb", "zx"]);
        assert_eq!(Solution::alien_order(words), "");
    }
}
