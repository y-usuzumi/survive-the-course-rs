trait TraitForAllTypes {
    fn my_i32(&self) -> i32;
}

impl<T> TraitForAllTypes for T {
    fn my_i32(&self) -> i32 {
        return 3;
    }
}

#[cfg(test)]
mod tests {
    use super::TraitForAllTypes;

    #[test]
    fn test_trait_for_all_types() {
        let c = 10;
        assert_eq!(c.my_i32(), 3);
    }
}
