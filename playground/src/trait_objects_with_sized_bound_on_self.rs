trait TraitObjectTest {
    fn new() -> Self
    where
        Self: Sized;
    fn display(&self) -> String;
}

struct TestStruct1;

impl TraitObjectTest for TestStruct1 {
    fn new() -> Self {
        TestStruct1
    }

    fn display(&self) -> String {
        "TestStruct1".to_string()
    }
}

struct TestStruct2;

impl TraitObjectTest for TestStruct2 {
    fn new() -> Self {
        TestStruct2
    }

    fn display(&self) -> String {
        "TestStruct2".to_string()
    }
}

fn display_all_trait_objects(objs: &[&dyn TraitObjectTest]) {
    for obj in objs {
        println!("{}", obj.display());
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_display_all_trait_objects() {
        let obj1 = &TestStruct1::new();
        let obj2 = &TestStruct2::new();
        let objs: Vec<&dyn TraitObjectTest> = vec![obj1, obj2];
        display_all_trait_objects(&objs);
    }
}
