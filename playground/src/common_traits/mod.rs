/// <b>Copy</b>
/// A type can implement Copy if all of its components implement Copy.
//
#[derive(Clone)]
struct S {
    val: String,
}
// Error: the trait `Copy` cannot be implemented for this type
// impl Copy for S {}
