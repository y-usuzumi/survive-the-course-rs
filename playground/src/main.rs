struct Pair<T> {
    first: T,
    second: T,
}

impl<T> Pair<T> {
    fn new(first: T, second: T) -> Self {
        Self { first, second }
    }
}

impl<T: Eq> Pair<T> {
    fn equals(&self) -> bool {
        self.first == self.second
    }
}

impl Pair<i32> {
    fn secret_thing(&self) {
        println!("secret: ({}, {})", self.first, self.second);
    }
}

fn main() {
    let string_pair = Pair::new("a", "a");
    println!("{}", string_pair.equals());
    let i32_pair = Pair::new(3, 4);
    i32_pair.secret_thing();
    // string_pair.secret_thing();  // Note: secret_thing is only implemented for Pair<i32>
}
